<?php

namespace modules\cpa\services;

use app\modules\cpa\services\ConnectApiService;
use yii\httpclient\Client;

class ConnectApiServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testCreateConnectApi()
    {
        $client = new ConnectApiService('http://0.0.0.0/');

        $this->assertInstanceOf(Client::class, $client->getClient());
    }
}