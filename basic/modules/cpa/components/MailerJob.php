<?php


namespace app\modules\cpa\components;


use yii\base\BaseObject;
use yii\di\Instance;
use yii\mail\MailerInterface;
use yii\mail\MessageInterface;
use yii\queue\JobInterface;
use yii\queue\Queue;

class MailerJob extends BaseObject implements JobInterface
{
    /** @var MessageInterface */
    public $message;
    /** @var string */
    public $mailer = 'mailer';

    /**
     * @param Queue $queue which pushed and is handling the job
     * @return void|mixed result of the job execution
     * @throws \yii\base\InvalidConfigException
     */
    public function execute($queue)
    {
        /** @var $mailer MailerInterface */
        $mailer = Instance::ensure($this->mailer, MailerInterface::class);

        $this->message->send($mailer);
    }
}