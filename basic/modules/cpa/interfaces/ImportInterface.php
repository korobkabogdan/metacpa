<?php


namespace app\modules\cpa\interfaces;


interface ImportInterface
{
    /**
     * @return array List Attributes for save in model.
     */
    public function load(): array;
}