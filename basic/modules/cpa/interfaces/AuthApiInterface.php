<?php


namespace app\modules\cpa\interfaces;


use yii\httpclient\Client;

interface AuthApiInterface
{
    /**
     * @return Client Customized Client
     */
    public function getClient(): Client;
}