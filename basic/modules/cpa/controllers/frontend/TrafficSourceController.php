<?php

namespace app\modules\cpa\controllers\frontend;

use app\modules\cpa\models\search\TrafficSourceSearch;
use Yii;
use yii\web\Controller;

/**
 * TrafficSourceController implements the CRUD actions for TrafficSource model.
 */
class TrafficSourceController extends Controller
{
    /**
     * Lists all TrafficSource models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrafficSourceSearch();
        $searchModel->visible = TrafficSourceSearch::VISIBLE_TRUE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
