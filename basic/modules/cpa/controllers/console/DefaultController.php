<?php


namespace app\modules\cpa\controllers\console;


use app\modules\cpa\interfaces\ImportCpaInterface;
use app\modules\cpa\models\CampaignsStat;
use app\modules\cpa\services\SaveService;
use app\modules\cpa\services\SendReportMailService;
use yii\console\Controller;
use yii\db\Connection;
use yii\helpers\Console;


class DefaultController extends Controller
{
    const LIMIT = 50;

    /** @var ImportCpaInterface */
    private $importService;

    /** @var SendReportMailService */
    private $senderService;

    /** @var Connection */
    private $_db;

    public function __construct($id, $module, ImportCpaInterface $importService, $config = [])
    {
        $this->_db = \Yii::$app->db;
        $this->importService = $importService;
        $this->senderService = new SendReportMailService();

        parent::__construct($id, $module, $config);
    }

    public function actionSaveImportData()
    {
        $this->stdout('START' . PHP_EOL, Console::FG_BLUE);

        $this->stdout('Загрузка данных...' . PHP_EOL, Console::FG_YELLOW);

        $data = $this->importService->load();

        $saveService = new SaveService($data);

        $total = count($data);
        $part = 0;

        $this->stdout("Получено строк - $total" . PHP_EOL, Console::FG_YELLOW);

        $this->stdout('Сохранение данных...' . PHP_EOL, Console::FG_YELLOW);

        $this->stdout('|');

        $transaction = $this->_db->beginTransaction();

        $saveService->truncate();
        try {
            while ($part < $total) {
                $partData = $saveService->getPartData($part, self::LIMIT);

                $saveService->save($partData);

                $part += self::LIMIT;
                $this->stdout('.');
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $this->stdout(PHP_EOL . $e->getMessage(), Console::FG_RED);
            $transaction->rollBack();
        }

        $this->stdout('|' . PHP_EOL);

        $this->stdout('Данные успешно сохранены!' . PHP_EOL, Console::FG_GREEN);

        $this->stdout('Отправляю отчет... ', Console::FG_YELLOW);

        try {
            $sent = $this->senderService->send([
                'clicks' => CampaignsStat::getSumField('clicks'),
                'leads' => CampaignsStat::getSumField('leads'),
                'revenue' => CampaignsStat::getSumField('revenue'),
                'event_2' => CampaignsStat::getSumField('event_2'),
            ]);
            if ($sent) {
                $this->stdout('отправлено' . PHP_EOL, Console::FG_GREEN);
            } else {
                $this->stdout('ошибка' . PHP_EOL, Console::FG_RED);
            }
        } catch (\Exception $e) {
            $this->stdout('ошибка' . PHP_EOL, Console::FG_RED);
            $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
        }


        $this->stdout('END' . PHP_EOL, Console::FG_BLUE);
    }
}