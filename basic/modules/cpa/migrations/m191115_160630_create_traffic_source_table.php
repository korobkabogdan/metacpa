<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%traffic_source}}`.
 */
class m191115_160630_create_traffic_source_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%traffic_source}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'visible' => $this->boolean(),
        ]);

        $this->createIndex(
            'idx-visible',
            '{{%traffic_source}}',
            'visible',
            false
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%traffic_source}}');
    }
}
