<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%campaigns_stat}}`.
 */
class m191115_162127_create_campaigns_stat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%campaigns_stat}}', [
            'id' => $this->primaryKey(),
            'campaignsId' => $this->integer(),
            'clicks' => $this->integer(),
            'leads' => $this->integer(),
            'revenue' => $this->integer(),
            'event_2' => $this->integer(),
            'create_date' => $this->integer(),
            'start_date' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-cs-campaignsId-campaigns-id',
            '{{%campaigns_stat}}',
            'campaignsId',
            '{{%campaigns}}',
            'campaignsId',
            'SET NULL',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%campaigns_stat}}');
    }
}
