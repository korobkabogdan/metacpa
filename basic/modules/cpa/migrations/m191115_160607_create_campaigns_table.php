<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%campaigns}}`.
 */
class m191115_160607_create_campaigns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%campaigns}}', [
            'id' => $this->primaryKey(),
            'campaignsId' => $this->integer(),
            'color' => $this->string(),
            'isBanned' => $this->boolean(),
            'name' => $this->string(),
            'status' => $this->smallInteger(),
            'groupId' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-c-groupId-group-id',
            '{{%campaigns}}',
            'groupId',
            '{{%group}}',
            'groupId',
            'SET NULL',
            'RESTRICT'
        );

        $this->createIndex(
            'unique-idx-campaignsId',
            '{{%campaigns}}',
            'campaignsId',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%campaigns}}');
    }
}
