<?php

/** @var $this \yii\web\View */
/** @var $clicks integer */
/** @var $leads integer */
/** @var $revenue integer */
/** @var $event_2 integer */
/** @var $date string */

?>
<h1>Отчет о получении данных</h1>

<div>Сумма по полям:</div>
<ul>
    <li>clicks: <?= $clicks ?></li>
    <li>leads: <?= $leads ?></li>
    <li>revenue: <?= $revenue ?></li>
    <li>event_2: <?= $event_2 ?></li>
</ul>
<div>Дата: <?= $date ?></div>
<div>Удачного дня!</div>
