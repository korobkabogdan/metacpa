<?php


namespace app\modules\cpa\services;


use app\modules\cpa\components\MailerJob;
use Yii;

class SendReportMailService extends SendMailBaseService
{
    const RECIPIENT = 'test@test.test';

    /**
     * @param array $options
     *
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     */
    public function send($options)
    {
        $clicks = $options['clicks'];
        $leads = $options['leads'];
        $revenue = $options['revenue'];
        $event_2 = $options['event_2'];

        $email = Yii::$app->params['adminEmail'];

        $message = $this->mailer->compose('@app/modules/cpa/mails/mail', [
            'clicks' => $clicks,
            'leads' => $leads,
            'revenue' => $revenue,
            'event_2' => $event_2,
            'date' => date('d.m.Y H:i'),
        ])
            ->setSubject('Отчет о получении данных')
            ->setFrom($email)
            ->setTo(self::RECIPIENT);

        $job = Yii::createObject([
            'class' => MailerJob::class,
            'message' => $message,
        ]);

        return $this->queue->push($job) ? true : false;
    }
}