<?php


namespace app\modules\cpa\services;


use app\modules\cpa\models\Campaigns;
use app\modules\cpa\models\CampaignsStat;
use app\modules\cpa\models\Group;
use app\modules\cpa\models\TrafficSource;
use yii\db\Connection;

class SaveService
{
    /** @var array */
    private $data;
    /** @var Connection */
    private $_db;

    public function __construct(array $data)
    {
        $this->data = $data;
        $this->_db = \Yii::$app->db;
    }

    /**
     * @param array $items
     * @throws \yii\db\Exception
     * @throws \Exception
     */
    public function save(array $items)
    {
        $dataForInsert = $this->getDataForInsert($items);

        $this->insertData($dataForInsert);
    }

    public function getPartData(int $part, int $limit): array
    {
        return array_slice($this->data, $part, $limit);
    }

    /**
     * @param array $items
     * @return array
     * @throws \Exception
     */
    private function getDataForInsert(array $items): array
    {
        $group = [];
        $campaigns = [];
        $campaignsStat = [];
        $trafficSource = [];

        foreach ($items as $item) {
            if (!$item->group_id) {
                $item->group_id = null;
            } else {
                $group[] = [
                    $item->group_id,
                    $item->group_name,
                ];
            }

            $campaigns[] = [
                $item->id,
                $item->color,
                $item->is_banned,
                $item->name,
                $item->status,
                $item->group_id,
            ];

            $campaignsStat[] = [
                $item->id,
                $item->clicks,
                $item->leads,
                $item->revenue,
                $item->event_2,
                $item->create_date,
                $item->start_date,
            ];

            $trafficSource[] = [
                $item->ts_name,
                random_int(0, 1),
            ];
        }

        return [
            'group' => array_values(array_unique($group, SORT_REGULAR)),
            'campaigns' => $campaigns,
            'campaignsStat' => $campaignsStat,
            'trafficSource' => $trafficSource,
        ];
    }

    /**
     * @param array $dataForInsert
     * @throws \yii\db\Exception
     */
    private function insertData(array $dataForInsert)
    {
        $sqlInsertGroup = $this->_db
            ->createCommand()
            ->batchInsert(Group::tableName(), [
                'groupId',
                'groupName',
            ], $dataForInsert['group'])
            ->getRawSql();

        if ($sqlInsertGroup) {
            $sqlInsertGroup = 'INSERT IGNORE' . mb_substr($sqlInsertGroup, strlen('INSERT'));
            $this->_db->createCommand($sqlInsertGroup)->execute();
        }

        $this->_db->createCommand()
            ->batchInsert(Campaigns::tableName(), [
                'campaignsId',
                'color',
                'isBanned',
                'name',
                'status',
                'groupId',
            ], $dataForInsert['campaigns'])
            ->execute();

        $this->_db->createCommand()
            ->batchInsert(CampaignsStat::tableName(), [
                'campaignsId',
                'clicks',
                'leads',
                'revenue',
                'event_2',
                'create_date',
                'start_date',
            ], $dataForInsert['campaignsStat'])
            ->execute();

        $this->_db->createCommand()
            ->batchInsert(TrafficSource::tableName(), [
                'name',
                'visible',
            ], $dataForInsert['trafficSource'])
            ->execute();
    }

    public function truncate()
    {
        CampaignsStat::deleteAll();
        Campaigns::deleteAll();
        Group::deleteAll();
        TrafficSource::deleteAll();
    }
}