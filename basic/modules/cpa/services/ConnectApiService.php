<?php


namespace app\modules\cpa\services;


use app\modules\cpa\interfaces\AuthApiInterface;
use Exception;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

class ConnectApiService implements AuthApiInterface
{
    private $client;

    /**
     * @param string $baseUrl
     * @throws InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws Exception
     */
    public function __construct(string $baseUrl)
    {
        $this->client = new Client([
            'transport' => CurlTransport::class,
            'baseUrl' => $baseUrl,
        ]);

        $response = $this->client->createRequest()
            ->setMethod('GET')
            ->send();

        if (!$response->isOk) {
            throw new Exception('Error Auth.');
        }
    }


    /**
     * @return Client Customized Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }
}