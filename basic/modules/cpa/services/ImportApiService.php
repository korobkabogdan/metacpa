<?php


namespace app\modules\cpa\services;


use app\modules\cpa\interfaces\AuthApiInterface;
use app\modules\cpa\interfaces\ImportCpaInterface;
use app\modules\cpa\interfaces\ImportInterface;
use Exception;
use yii\httpclient\Client;

/**
 * Class ImportApiService
 * @package app\modules\cpa\services
 *
 * @property string $route
 */
class ImportApiService implements ImportInterface, ImportCpaInterface
{
    /** @var string */
    public $route;
    /** @var Client */
    private $_client;

    /**
     * ImportApiService constructor.
     * @param AuthApiInterface $authApi
     */
    public function __construct(AuthApiInterface $authApi)
    {
        $this->_client = $authApi->getClient();
    }

    /**
     * @return array List Attributes for save in model.
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function load(): array
    {
        $response = $this->_client->createRequest()
            ->setUrl($this->route)
            ->setMethod('GET')
            ->send();

        if (!$response->isOk) {
            throw new Exception('Error load attributes.');
        }

        return json_decode($response->content);
    }
}