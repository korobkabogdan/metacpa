<?php


namespace app\modules\cpa\services;


use yii\di\Instance;
use yii\queue\Queue;
use yii\swiftmailer\Mailer;

abstract class SendMailBaseService
{
    /** @var Queue $queue */
    public $queue;

    /** @var Mailer $mailer */
    protected $mailer;

    /** @var array */
    public $mailerConfig = [
        'class' => '\yii\swiftmailer\Mailer',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => '',
            'encryption' => ''
        ]
    ];

    /**
     * SendMailService constructor.
     * @param string $queue
     * @param array $mailerConfig
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct(string $queue = 'queue', array $mailerConfig = [])
    {
        $this->queue = Instance::ensure($queue, Queue::class);
        $this->mailerConfig['transport'] = array_merge(
            $this->mailerConfig['transport'],
            $mailerConfig
        );

        $this->mailer = \Yii::createObject($this->mailerConfig);
    }

    /**
     * @return Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param array $options
     *
     * @return boolean
     */
    abstract public function send($options);
}