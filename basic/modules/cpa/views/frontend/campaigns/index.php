<?php

use app\modules\cpa\models\Group;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpa\models\search\Campaigns */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campaigns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaigns-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'campaignsId',
            'color',
            'isBanned:boolean',
            'name',
            'status:boolean',
            [
                'attribute' => 'groupId',
                'filter' => Group::getList(),
                'value' => 'group.groupName',
            ],
            [
                'attribute' => 'clicks',
                'value' => 'stat.clicks',
            ],
            [
                'attribute' => 'leads',
                'value' => 'stat.leads',
            ],
            [
                'attribute' => 'revenue',
                'value' => 'stat.revenue',
            ],
            [
                'attribute' => 'event_2',
                'value' => 'stat.event_2',
            ],
            [
                'attribute' => 'create_date',
                'value' => 'stat.create_date',
                'format' => 'date',
            ],
            [
                'attribute' => 'start_date',
                'value' => 'stat.start_date',
                'format' => 'date',
            ],
        ],
    ]); ?>


</div>
