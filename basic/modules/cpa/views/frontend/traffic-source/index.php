<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cpa\models\search\TrafficSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Traffic Sources';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traffic-source-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'visible:boolean',
        ],
    ]); ?>


</div>
