<?php

namespace app\modules\cpa\models;

/**
 * This is the model class for table "traffic_source".
 *
 * @property int $id
 * @property string $name
 * @property int $visible
 */
class TrafficSource extends \yii\db\ActiveRecord
{
    const VISIBLE_TRUE = 1;
    const VISIBLE_FALSE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%traffic_source}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visible'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'visible' => 'Visible',
        ];
    }
}
