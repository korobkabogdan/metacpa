<?php

namespace app\modules\cpa\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "group".
 *
 * @property int $id
 * @property int $groupId
 * @property string $groupName
 *
 * @property Campaigns[] $campaigns
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['groupId'], 'integer'],
            [['groupName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'groupId' => 'Group ID',
            'groupName' => 'Group Name',
        ];
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'groupId', 'groupName');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaigns()
    {
        return $this->hasMany(Campaigns::className(), ['groupId' => 'id']);
    }
}
