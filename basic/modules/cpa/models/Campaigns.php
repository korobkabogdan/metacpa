<?php

namespace app\modules\cpa\models;

/**
 * This is the model class for table "campaigns".
 *
 * @property int $id
 * @property int $campaignsId
 * @property string $color
 * @property int $isBanned
 * @property string $name
 * @property int $status
 * @property int $groupId
 *
 * @property CampaignsStat $stat
 * @property Group $group
 */
class Campaigns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%campaigns}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campaignsId', 'isBanned', 'status', 'groupId'], 'integer'],
            [['color', 'name'], 'string', 'max' => 255],
            [
                ['groupId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Group::className(),
                'targetAttribute' => ['groupId' => 'groupId']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaignsId' => 'Campaigns ID',
            'color' => 'Color',
            'isBanned' => 'Is Banned',
            'name' => 'Name',
            'status' => 'Status',
            'groupId' => 'Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['groupId' => 'groupId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStat()
    {
        return $this->hasOne(CampaignsStat::className(), ['campaignsId' => 'campaignsId']);
    }
}
