<?php

namespace app\modules\cpa\models;

/**
 * This is the model class for table "campaigns_stat".
 *
 * @property int $id
 * @property int $campaignsId
 * @property int $clicks
 * @property int $leads
 * @property int $revenue
 * @property int $event_2
 * @property int $create_date
 * @property Campaigns $campaigns
 * @property int $start_date
 */
class CampaignsStat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%campaigns_stat}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clicks', 'leads', 'revenue', 'event_2', 'create_date', 'start_date'], 'integer'],
            [
                ['campaignsId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Campaigns::className(),
                'targetAttribute' => ['campaignsId' => 'campaignsId']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clicks' => 'Clicks',
            'leads' => 'Leads',
            'revenue' => 'Revenue',
            'event_2' => 'Event 2',
            'create_date' => 'Create Date',
            'start_date' => 'Start Date',
        ];
    }

    public static function getSumField(string $field)
    {
        return self::find()
            ->sum($field);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaigns()
    {
        return $this->hasOne(Campaigns::className(), ['campaignsId' => 'campaignsId']);
    }
}
