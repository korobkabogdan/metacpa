<?php

namespace app\modules\cpa\models\search;

use app\modules\cpa\models\Campaigns;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Campaigns represents the model behind the search form of `app\modules\cpa\models\Campaigns`.
 */
class CampaignsSearch extends Campaigns
{
    public $clicks;
    public $leads;
    public $revenue;
    public $event_2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'campaignsId',
                    'isBanned',
                    'status',
                    'clicks',
                    'leads',
                    'revenue',
                    'event_2',
                ],
                'integer'
            ],
            [
                [
                    'color',
                    'name',
                    'groupId',
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $addSort = [
            'clicks',
            'leads',
            'revenue',
            'event_2',
            'create_date',
            'start_date',
        ];

        $dataProvider->setSort([
            'attributes' => array_merge($dataProvider->getSort()->attributes, $addSort),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('stat');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'campaignsId' => $this->campaignsId,
            'isBanned' => $this->isBanned,
            'status' => $this->status,
            'groupId' => $this->groupId,
            'clicks' => $this->clicks,
            'leads' => $this->leads,
            'revenue' => $this->revenue,
            'event_2' => $this->event_2,
        ]);

        $query->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
