<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'queue'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            'db' => 'db',
            'tableName' => '{{%queue}}',
            'channel' => 'default',
            'mutex' => \yii\mutex\MysqlMutex::class,
            'deleteReleased' => false,
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'transport' => [
                'class' => \Swift_SmtpTransport::class,
                'host' => getenv('SMTP_HOST'),
                'username' => getenv('SMTP_USERNAME'),
                'password' => getenv('SMTP_PASSWORD'),
                'port' => getenv('SMTP_PORT'),
                'encryption' => getenv('SMTP_ENCRYPTION'),
            ],
            'useFileTransport' => YII_DEBUG, // @runtime/mail/
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationTable' => '{{%migration}}',
            'useTablePrefix' => true,
            'interactive' => false,
            'migrationPath' => [
                '@app/migrations',
                '@app/modules/cpa/migrations',
            ],
            'migrationNamespaces' => [
                '\yii\queue\db\migrations',
            ],
        ],

    ],
    'container' => [
        'singletons' => [
            \app\modules\cpa\interfaces\AuthApiInterface::class => function () {
                $baseUrl = 'http://209.97.133.143/tz/api.php';

                return new \app\modules\cpa\services\ConnectApiService($baseUrl);
            },
        ],
        'definitions' => [
            \app\modules\cpa\interfaces\ImportCpaInterface::class => function () {
                return Yii::createObject([
                    'class' => \app\modules\cpa\services\ImportApiService::class,
                    'route' => '?key=20e62b46af322affca0d9bccb7a',
                ]);
            },
            \yii\mail\MailerInterface::class => function () {
                return Yii::$app->getMailer();
            },
        ],
    ],
    'modules' => [
        'cpa' => [
            'class' => \app\modules\cpa\Module::class,
            'controllerNamespace' => '\app\modules\cpa\controllers\console',
        ],
    ],
];
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
