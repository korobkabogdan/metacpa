# MetaCPA
Тестовое задание

Установка:
1. Установить Vagrant
2. vagrant up

Cron tasks:
1. Queue: * * * * * /usr/bin/php /app/yii queue/run
2. Import: 0 0 * * * /usr/bin/php /app/yii cpa/default/save-import-data (Запускаем каждый день в 12 ночи)

